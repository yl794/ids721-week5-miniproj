# ids-week5-miniProj

This repo contains code to build a simple service to calculate the maximum quantity in the table.

## Steps

1. Install Rust, Cargo Lambda and AWS CLI
2. Create a new Rust project by running `cargo lambda new week5_lambda`
3. Create a MySQL database on AWS
![](1.png)
4. implement Lambda Function in the src/main.rs
5. Build the Lambda Function using `cargo lambda build`
6. Set IAM roles to enable deployment
7. Deploy the Lambda Function using `cargo lambda deploy` 
8. add an API Gateway as trigger
![](2.png)
9. successfully invoke the Lambda Function
![](3.png)
