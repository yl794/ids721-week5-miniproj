use mysql_async::{prelude::Queryable, Row, Pool};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let pool = Pool::new("mysql://admin:12345@week5.c2uf4s67yu89.us-east-1.rds.amazonaws.com:3306/week5");
    let mut conn = pool.get_conn().await?;
    let rows: Vec<Row> = conn.query("SELECT max(quantity) AS max FROM week5").await?;
    let result: Option<i64> = rows.get(0).and_then(|row| row.get("max"));
    match result {
        Some(max) => println!("Maximum quantity: {}", max),
        None => println!("No data found or max is NULL"),
    }

    Ok(())
}
